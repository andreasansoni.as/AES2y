close all
clear all

f = @(x) 1-5*x;
u_ex = @(x) ((x-1).^3)/6;
alpha = 0;
beta = 1/6;
x0 = 1; xf = 2;

set(0,'defaultTextInterpreter','latex')




%% SOLUTION COMPARISON
z = [x0:0.001:xf];
figure
title('Consistent Error Convergence')
plot(z, u_ex(z));
grid on
hold on


h_vect = 0.1*2.^-[0:3];
e_h_vect = [];
e_h_vect_incons = [];


for i = 1:length(h_vect)
    
    h = h_vect(i);
    N = (xf-x0)/h - 1;
    knots_int = [x0+h:h:xf-h]';
    
    A = (1/(h^2)) * (diag(-ones(N-1,1),-1) + 2*eye(N) + diag(-ones(N-1,1),1) );
    b = f(knots_int);
    b(1) = b(1) + (alpha/(h^2));
    b(end) = b(end) + (beta/(h^2));
    
    u = A\b;
    u = [alpha; u; beta];
    
    x = [x0; knots_int; xf];
   
    % inconsistent error
    e_h_incons = max(abs(u_ex(x) - u));
    e_h_vect_incons = [e_h_vect_incons e_h_incons];
    plot(x,u)
    
    % consistent error
    pz = interp1(x, u, z);
    e_h = max(abs(u_ex(z) - pz));
    e_h_vect = [e_h_vect e_h];
    
end

legend('u_{ex}(x)', 'u_{h=0.1}', 'u_{h=0.05}', 'u_{h=0.025}', 'u_{h=0.0125}')
title('Comparison between \textbf{approximate} solutions and \textbf{exact} solution')
xlabel('$x$')
ylabel('$u(x)$')


%% ZOOMED PLOT
axes('position',[.2 .60 .2 .25])
box on
x_min = 1.632;
x_max = 1.633;
z = [x0:0.001:xf];
title('Consistent Error Convergence')
plot(z, u_ex(z));
grid on
hold on


h_vect = 0.1*2.^-[0:3];

for i = 1:length(h_vect)
    
    h = h_vect(i);
    N = (xf-x0)/h - 1;
    knots_int = [x0+h:h:xf-h]';
    
    A = (1/(h^2)) * (diag(-ones(N-1,1),-1) + 2*eye(N) + diag(-ones(N-1,1),1) );
    b = f(knots_int);
    b(1) = b(1) + (alpha/(h^2));
    b(end) = b(end) + (beta/(h^2));
    
    u = A\b;
    u = [alpha; u; beta];
    
    x = [x0; knots_int; xf];
    plot(x,u)

end

xlim([x_min x_max])




%% CONVERGENCE ANALYSIS
figure
title('Error Convergence')

subplot(1,2,1)
loglog(h_vect, e_h_vect_incons)
grid on
hold on
loglog(h_vect, h_vect.^2,'--k')
legend('e_{h}','h^{2}')
title('\textbf{\underline{Inconsistent} Results}')
xlabel('$h$')
ylabel('$e_{h}$')


subplot(1,2,2)
loglog(h_vect, e_h_vect)
grid on
hold on
loglog(h_vect, h_vect.^2,'--k')
legend('e_{h}','h^{2}')
title('\textbf{Consistent Results}')
xlabel('$h$')
ylabel('$e_{h}$')

