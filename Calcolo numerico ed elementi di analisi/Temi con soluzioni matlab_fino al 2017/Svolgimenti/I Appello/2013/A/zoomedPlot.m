function [ ] = zoomedPlot( x, y, x_min, x_max)

axes('position',[.65 .175 .2 .25])

if isOn

h = (x(end)-x(1))/length(x)
x_rangeOfInterest = (x > x_min) & (x < x_max); % range of times
x_zoom = x(x_rangeOfInterest);
y_zoom = y(round((x_zoom(1)-x(1))/h):round((x_zoom(end)-x(1))/h));
plot(x_zoom, y_zoom) % plot on new axes
hold on
grid on
axis tight
title('Zoomed Area')

return

