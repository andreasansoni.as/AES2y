close all

%% 4.2
A = [8 1 1; 3 1 -1; 1 0 -3];
B = [0 50/99 0; 1 0.5 0; 1 1 1];

gershcircles(A)

pause

close all
gershcircles(B)

pause
close all



%% 4.4
tol = 1e-7;
itermax = 1e+4;

[lambdas_A, k_A] = qrbasic( A, tol, itermax)

[lambdas_B, k_B] = qrbasic( B, tol, itermax)



% Si noti che la convergenza per la matrice B è molto più lenta per il
% fatto che i suoi autovalori sono molto ravvicinati, a differenza degli
% autovalori di A.
% Ciò implica che gli elementi sotto la diagonale principale di B tendano
% lentamente a zero, ossia le B^(k) tendono lentamente a diventare upper
% triangular