clear all
close all

%% 2-3
f = @(x) 1 ./ (1 + exp(-5*x));
a = 0; b = 1;
n = 3;

h = (b-a)/n;

z = [a:0.001:b];
subplot(1,2,1)
plot(z,f(z))
grid on
hold on

x = [a:h:b];
pz = interp1(x, f(x), z);
plot(z, pz)

legend('f(x)', '\Pi_{3}f(x)')

E_f = abs(pz - f(z));
subplot(1,2,2)
plot(z, E_f)
grid on
legend('E_{3}f(x)')

%% 4-5
n_vect = 2.^[2:5];

figure
z = [a:0.001:b];
subplot(1,2,1)
plot(z,f(z))
grid on
hold on

h_vect = [];
e_h_vect = [];

for i = 1:length(n_vect)
    
    n = n_vect(i);
    h = (b-a)/n;
    h_vect = [h_vect h];
   
    x = [a:h:b];
    pz = interp1(x, f(x), z);
    plot(z, pz)
    
    e_h = max(abs(pz - f(z)));
    e_h_vect = [e_h_vect e_h];

end

legend('f(x)', '\Pi_{4}f(x)', '\Pi_{8}f(x)', '\Pi_{16}f(x)', '\Pi_{32}f(x)')


subplot(1,2,2)
loglog(h_vect, e_h_vect)
grid on
hold on
loglog(h_vect, h_vect.^2, '--k')
legend('e_{h}', 'h^{2}')

%%
p_appr = log(e_h_vect(end-1)/e_h_vect(end)) / log(h_vect(end-1)/h_vect(end))