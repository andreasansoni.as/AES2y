%% 1.1
n = 30;
A = 50*eye(n) + diag([1:n-1],-1) + diag([1:n-1],1) + 0.1*diag([1:n-2],-2) + 0.1*diag([1:n-2],2);

x_exact = ones(n,1);
P = 4*eye(n);

%Ax=b -> b=A*x;
b = A*x_exact;

%% 1.2
x0 = 2000*ones(n,1);
tol = 1e-5;
nmax = 1e+5;
alpha = 0.075;

[ ~, k_staticRand ] = richardson( A, b, P, x0, tol, nmax, alpha )

%% 1.2

P_inv = diag(1./diag(P));

if (min(eig(A)) > 0) & (A==A')
    disp('La matrice A è SDP')
    
    lambdas = eig(P_inv*A);
    
    % Essendo A e P entrambe SDP:
    alpha_opt = 2/(min(lambdas) + min(lambdas))
    
    
    [ ~, k_staticOpt ] = richardson( A, b, P, x0, tol, nmax, alpha_opt )

end

    [ ~, k_dynamic ] = richardson( A, b, P, x0, tol, nmax )
