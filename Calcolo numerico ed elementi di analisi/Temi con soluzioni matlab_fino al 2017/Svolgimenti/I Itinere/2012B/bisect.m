function [ x_vect, k, itermax] = bisect( f,a,b,tol )

if nargin == 3
   tol = 1e-10; 
end

itermax = ceil(log2((b-a)/tol)-1);

x = (a+b)/2;
err = abs(f(x));
k = 0;

x_vect = [x];

while k < itermax && err > tol
    
    k = k+1;
    
    if f(x) == 0 
       break
    end
    
    if f(a)*f(x) < 0
        b = x;
    elseif f(x)*f(b) < 0
        a = x;
    end
        
    x = (a+b)/2;
    x_vect = [x_vect x];
    
    err = abs(f(x));
  
end

end

