f = @(t,y) -(exp(-t)+1).*(y-1);
t0 = 0;
tf = 10;
y0 = 0;

y_ex = @(t) -exp(exp(-t)-1).*exp(-t)+1;
z = [t0:0.001:tf];
figure
plot(z,y_ex(z))
grid on
hold on

h_vect = 2.^-[2:5];
e_h_vect = [];
u_f_vect = [];
for k = 1:length(h_vect)
    h = h_vect(k);
    [t_h,u_h] = beuler(f,t0,tf,y0,h);
    plot(t_h, u_h)
    
    u_f_vect = [u_f_vect u_h(end)];
    
    e_h = max(abs(u_h-y_ex(t_h)));
    e_h_vect = [e_h_vect e_h];
end

u_f_vect

legend('y_{ex}(t)', 'u_{h=0.25}', 'u_{h=0.125}', 'u_{h=0.0625}', 'u_{h=0.03125}')


figure
loglog(h_vect, e_h_vect)
grid on
hold on
loglog(h_vect, h_vect, '--k')

legend('e_{h}', 'h^{1}')

p_appr = log(e_h_vect(end-1)/e_h_vect(end)) / log(h_vect(end-1)/h_vect(end))

