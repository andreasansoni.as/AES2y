function [ t_h, u_h ] = Crank_Nicolson( f, y0, t0, tf, h )

Nh = (tf-t0)/h;

t_h = [t0:h:tf]';
u_h = [y0];

itermax = 100;
tol = 1e-5;

for n = 0:Nh-1
    
    t_n = n*h;
    t_np1 = (n+1)*h;
    
    u_n = u_h(end);
    
    
    phi = @(u_np1) u_n +  (h/2)*(f(t_n, u_n) + f(t_np1, u_np1) );
    [u_np1, ~] = fixedPt(u_n, phi, itermax, tol);
    
    u_h = [u_h; u_np1];    

end

