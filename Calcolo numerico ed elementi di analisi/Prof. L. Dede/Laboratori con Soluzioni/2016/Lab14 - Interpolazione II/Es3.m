% Esercizio 3
clear all
close all

fprintf( '\n######## Esercizio 3 ########\n')

%%% PUNTO 1 %%%

fun =@(x) abs ( x - pi / 12 );
a = -1;
b = 1;
x= linspace ( a , b , 1000);
f= fun ( x ) ;
figure( 1 )
axes ( 'FontSize' ,12)
plot( x, f, 'k--' , 'linewidth' , 4)
title ( ' Grafico di : f ( x ) = abs ( x - pi / 12 ) ' ) ;
xlabel ( ' x ' )
ylabel ( ' f ( x ) ' )

%%% PUNTO 2 %%%

ytI = [ ] ;
errI = [ ] ;
g = [5 10 15 ] ;
for n = g
    % Spaziatura uniforme
    h = ( b - a ) / n ;
    xnod = a : h : b ;
    fnod = fun ( xnod ) ;

    % Interpolazione

    P = polyfit( xnod , fnod , n ) ;
    yI = polyval (P, x ) ;

    % Salvataggio dei dati e calcolo dell'errore
    ytI = [ ytI ; yI ] ;
    errI = [ errI ; abs( yI - f ) ] ;
end

err_max = max( errI , [ ] , 2 )';
figure(2)
axes ( 'FontSize' ,12)
plot ( x , f, 'k--' , 'linewidth' , 4)
hold on
plot ( x , ytI , 'linewidth' , 2)
title ( ' Interpolazione di Lagrange ( nodi equispaziati ) ' ) ;
xlabel ( ' x ' )
ylabel ( ' f ( x ) ' )
legend ( strvcat ( ' f ( x ) ' , ...
    [ repmat(' n = ',length(g),1), num2str(g') ] ) , 2 ,...
    'location','NorthWest')
fprintf ( '\nPunto 2:\n ' )
fprintf ( ' IL grado %3d errore massimo %f \n ' , [ g ; err_max ] )


fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 3 %%%

ytI = [ ] ;
errI = [ ] ;
g = 2:25;
for n = g
    
    % Spaziatura di Chebychev
    i = 0: n ;
    xloc = -cos ( i*pi / n ) ;
    xnod = ( a + b ) / 2 + ( b - a ) / 2*xloc ;
    fnod = fun ( xnod ) ;

    % Interpolazione

    P = polyfit( xnod , fnod , n ) ;
    yI = polyval (P, x ) ;

    % Salvataggio dei dati e calcolo dell'errore
    ytI = [ ytI ; yI ] ;
    errI = [ errI ; abs( yI - f ) ] ;
end

err_max = max( errI , [ ] , 2 )';   

figure(3)
axes ( 'FontSize' ,12)

for i =1: length(g)
    plot(  x, f, 'k--' , 'linewidth' , 4)
    hold on
    plot ( x, ytI ( 1 : i , : ) )
    title ( ' Interpolazione di Lagrange ( nodi di Chebychev ) ' ) ;
    xlabel ( ' x ' )
    ylabel ( ' f ( x ) ' )
    legend ( strvcat ( ' f ( x ) ' , ...
        [ repmat(' n = ',i,1), num2str(g(1:i)') ] ) , 2 ,...
        'location','NorthEastOutside')
    hold off
end
fprintf ( '\nPunto 3:\n ' )
fprintf( ' IL grado %3d errore massimo %f \n ' , [ g ; err_max ] )

fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 4 %%%

y1 = 1 ./ g ;
y2 = 1 ./ ( g.^2 ) ;
figure(4)
axes ( 'FontSize' ,12)
loglog ( g , err_max, 'b-' , g , y1 , 'k--' , g , y2 , 'k--' , 'linewidth' , 2)
title( ' Errore dell'' interpolazione di Lagrange ( nodi di Chebychev ) ' ) ;
xlabel ( ' Grado ' )
ylabel ( ' Errore massimo ' )
legend ( ' Errore ' , ' Rette di riferimento ' , 1)

fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 5 %%%

ytI = [ ] ;
errI = [ ] ;
g = 2.^[1:7];
for n = g
    % Spaziatura uniforme
    h = ( b - a ) / n ;
    xnod = a : h : b ;
    fnod = fun ( xnod ) ;
    
    % Interpolazione

    yI=interp1(xnod,fnod,x);

    % Salvataggio dei dati e calcolo dell'errore
    ytI = [ ytI ; yI ] ;
    errI = [ errI ; abs( yI - f ) ] ;
end

err_max_ICL = max( errI , [ ] , 2 )';   

figure(5)
axes ( 'FontSize' ,12)

for i =1: length(g)
    plot(  x, f, 'k--' , 'linewidth' , 4)
    hold on
    plot ( x, ytI ( 1 : i , : ) )
    title ( ' Interpolazione Composita Lineare (nodi equispaziati)' ) ;
    xlabel ( ' x ' )
    ylabel ( ' f ( x ) ' )
    legend ( strvcat ( ' f ( x ) ' , ...
        [ repmat(' n = ',i,1), num2str(g(1:i)') ] ) , 2 ,...
        'location','NorthEastOutside')
    hold off
end
fprintf ( '\nPunto 5:\n ' )
fprintf( ' ICL grado %3d errore massimo %f \n ' , [ g ; err_max_ICL ] )

fprintf('\n Press any key to continue...\n');pause;

%Rette di riferimento
h = ( b - a ) ./ g ;
y1 = h ;
y2 = h.^2 ;
figure(6);

axes ( 'FontSize' ,12)
loglog ( h , err_max_ICL, 'b-' , h , y1 , 'k--' , h , y2 , 'k--' , 'linewidth' , 2)
title( ' Errore dell'' interpolazione Composita Lineare (nodi equispaziati)' ) ;
xlabel ( '  Ampiezza : H = ( b − a ) / g ' )
ylabel ( ' Errore massimo ' )
legend ( ' Errore ' , ' Rette di riferimento ' , 1, 'location','SouthEast')

fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 6 %%%

gIL = 2: 25;
gICL = 2 .^( 1 : 5 ) ;
y1 = 1 ./ gICL ;
y2 = 1 ./ ( gICL.^2 ) ;
% Confronto IL − ICL
figure(7)
axes ( 'FontSize' ,12)
loglog ( gIL , err_max, 'b-' , gICL , err_max_ICL ( 1 : 5 ) , 'r-' , gICL , y1 , 'k-- ' , gICL , y2 , 'k-- ' , 'linewidth' , 2)
title( ' Confronto dell'' errore massimo tra IL ( con Chebychev ) e ICL ' ) ;
xlabel ( ' Grado ' )
ylabel ( ' Errore massimo ' )
legend ( ' IL ' , ' ICL ' , ' Rette di riferimento ' , 1, 'location','SouthWest')
