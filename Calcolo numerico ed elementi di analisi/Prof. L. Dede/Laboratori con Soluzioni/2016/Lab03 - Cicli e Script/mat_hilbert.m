function A = mat_hilbert( n )

% A = mat_hilbert( n )
% Funzione che restituisce la matrice di Hilbert di dimensione n x n
%
%   Input  : n  =  dimensione della matrice quadrata
%   Output : A  =  matrice di Hilbert

A = [];

if n <= 0
    disp('Errore:');
    disp('    n deve essere positivo !');
    return
end

for i = 1 : n
    for j = 1 : n
        A( i, j ) = 1 / ( i + j - 1 );
    end
end

