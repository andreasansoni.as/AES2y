close all


%% 1

mu = 1;
etha = 0;
alpha = 1;
beta = 1;
x0 = 0;
xf = 1;


N = 19;
h = (xf-x0)/(N+1);
knots = [x0:h:xf]';



A = mu/(h^2) * (diag(-ones(N-1,1), -1) + diag(2*ones(N,1), 0) + diag(-ones(N-1,1), 1)) + etha/(2*h) * (diag(-ones(N-1,1), -1) + diag(-ones(N-1,1), 1));

f = @(x) exp(3*x).*(etha - 4*mu + (etha+3*mu)*x + 3*(3*mu - etha)*x.^2);
u_ex = @(x) exp(3*x).*(x-x.^2) + 1;

b = f(knots(2:end-1));
b(1) = b(1) + ((mu/(h^2)) + (etha/(2*h))) * alpha;
b(end) = b(end) + ((mu/(h^2)) - (etha/(2*h))) * beta;


[ L, U, x ] = thomas(A,b);
u = x;
u = [alpha; u; beta];


figure
subplot(2,2,1)
plot(knots, u, '-o')
grid on
hold on
x_axis = [x0:0.01:xf];
plot(knots, u_ex(knots), '-r')
legend('u', 'u_{ex}')
title('Diffusione')




%% 2

mu = 1e-2;
etha = 1;
alpha = 1;
beta = 1;
x0 = 0;
xf = 1;


N = 19;
h = (xf-x0)/(N+1);
knots = [x0:h:xf]';



A = mu/(h^2) * (diag(-ones(N-1,1), -1) + diag(2*ones(N,1), 0) + diag(-ones(N-1,1), 1)) + etha/(2*h) * (diag(-ones(N-1,1), -1) + diag(ones(N-1,1), 1));

f = @(x) exp(3*x).*(etha - 4*mu + (etha+3*mu)*x + 3*(3*mu - etha)*x.^2);
u_ex = @(x) exp(3*x).*(x-x.^2) + 1;

b = f(knots(2:end-1));
b(1) = b(1) + ((mu/(h^2)) + (etha/(2*h))) * alpha;
b(end) = b(end) + ((mu/(h^2)) - (etha/(2*h))) * beta;


[ L, U, x ] = thomas(A,b);
u = x;
u = [alpha; u; beta];


subplot(2,2,2)
plot(knots, u, '-o')
grid on
hold on
x_axis = [x0:0.01:xf];
plot(knots, u_ex(knots), '-r')
legend('u', 'u_{ex}')
title('D-T (\mu=10^{-2})')




%% 3 OSCILLAZIONI NUMERICHE

mu = 1e-4;
etha = 1;
alpha = 1;
beta = 1;
x0 = 0;
xf = 1;


N = 19;
h = (xf-x0)/(N+1);
knots = [x0:h:xf]';



A = mu/(h^2) * (diag(-ones(N-1,1), -1) + diag(2*ones(N,1), 0) + diag(-ones(N-1,1), 1)) + etha/(2*h) * (diag(-ones(N-1,1), -1) + diag(ones(N-1,1), 1));

f = @(x) exp(3*x).*(etha - 4*mu + (etha+3*mu)*x + 3*(3*mu - etha)*x.^2);
u_ex = @(x) exp(3*x).*(x-x.^2) + 1;

b = f(knots(2:end-1));
b(1) = b(1) + ((mu/(h^2)) + (etha/(2*h))) * alpha;
b(end) = b(end) + ((mu/(h^2)) - (etha/(2*h))) * beta;


[ L, U, x ] = thomas(A,b);
u = x;
u = [alpha; u; beta];


subplot(2,2,3)
plot(knots, u, '-o')
grid on
hold on
x_axis = [x0:0.01:xf];
plot(knots, u_ex(knots), '-r')
legend('u', 'u_{ex}')
title('D-T (\mu=10^{-4}) D.F.C.')

Pe = abs(etha)*(xf-x0)/(2*mu)




%% 4 UPWIND

mu = 1e-4;
etha = 1;
alpha = 1;
beta = 1;
x0 = 0;
xf = 1;


N = 19;
h = (xf-x0)/(N+1);
knots = [x0:h:xf]';



A = mu/(h^2) * (diag(-ones(N-1,1), -1) + diag(2*ones(N,1), 0) + diag(-ones(N-1,1), 1)) + etha/(h) * (diag(-ones(N-1,1), -1) + diag(ones(N,1), 0));

f = @(x) exp(3*x).*(etha - 4*mu + (etha+3*mu)*x + 3*(3*mu - etha)*x.^2);
u_ex = @(x) exp(3*x).*(x-x.^2) + 1;

b = f(knots(2:end-1));
b(1) = b(1) + ((mu/(h^2)) + (etha/(h))) * alpha;
b(end) = b(end) + (mu/(h^2)) * beta;


[ L, U, x ] = thomas(A,b);
u = x;
u = [alpha; u; beta];


subplot(2,2,4)
plot(knots, u, '-o')
grid on
hold on
x_axis = [x0:0.01:xf];
plot(knots, u_ex(knots), '-r')
legend('u', 'u_{ex}')
title('D-T (\mu=10^{-4}) UPWIND')






