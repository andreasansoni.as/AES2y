function [ I ] = pmedcomp( a, b, M, f )

H = (b-a)/M;

x_vect = [a:H:b]

x_med_vect = x_vect(1:length(x_vect)-1) + (H/2)*ones(1,length(x_vect)-1)

I = H*sum(f(x_med_vect))

