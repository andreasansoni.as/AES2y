function [lambda,x,k]=invpowershift(A, s, tol,itermax,x0)


y = x0/norm(x0);
lambda = y'*A*y;
err = tol + 1;
k = 0;

A_s = A - s*eye(length(diag(A)));
[L,U,P]=lu(A_s); % Calcolata una volta sola e riutilizzata

while (err > tol) && (k<itermax)
   k = k + 1;
   
   y1=fwsub(L,P*y);
   x=bksub(U,y1);
   y= x/norm(x);
   
   lambda_old = lambda;
   lambda = y'*A*y;
   err = abs(lambda - lambda_old)/abs(lambda);
end




end
