clc
clear

x1=linspace(0,pi,1000);
x2=linspace(-pi,0,1000);
f=@(x) (x.^2+x)./6;
g=@(x) sin(x);
plot(x1,f(x1),'r');
hold on;
plot(x2,g(x2),'k');
plot([-pi pi],[ 0 0]);
