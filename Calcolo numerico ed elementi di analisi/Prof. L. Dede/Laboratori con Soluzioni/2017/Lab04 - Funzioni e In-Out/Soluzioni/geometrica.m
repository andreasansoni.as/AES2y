function a_bar = geometrica(a)
  n = numel(a);  prodotto = 1;
  for ii = 1:n
    prodotto = prodotto*a(ii);
  end  
  a_bar = prodotto^(1/n);
end